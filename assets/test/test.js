QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "3", "1<3 - the first agrument is 'truthy', so we pass!");
});

QUnit.test('Testing calculateArea function with several sets of inputs', function (assert) {
    assert.equal(Accler(10,5), 2, 'Tested with two relatively small positive numbers');
    assert.equal(Accler(20,4),5,'Tested with two big numbers')
   assert.throws(Accler(0,0),0, 'Passing a zero value');
   assert.throws(Accler(3,-2),NaN, 'Passing a negative value rasies an error');
//assert.equal(Accler.calculateArea(500, 500), 10000, 'Tested with two large positive numbers. Any arguments greater than 100 will be set to 100.');
//     assert.throws(function () { App.calculateArea(null); }, /The given argument is not a number/, 'Passing in null correctly raises an Error');
//         //throws( block                                    [, expected ] [, message ] ) 
//     assert.throws(function () { App.calculateArea("Christine","Christine"); }, /The given argument is not a number/, 'Passing in a string correctly raises an Error');
// 
});

